#!/usr/bin/env bash


set -e # exit immediately if one command fails
set -f # disable filename globbing (use "shopt -s failglob" if globbing is needed)
#shopt -s failglob
set -u          # treat unset variables as errors
set -o pipefail # fail the whole pipe if one command fails
#set -x # show every executed command

source .venv_klash/bin/activate

klash-update-repos
klash-summit-merge
klash-create-missing-po
klash-edit-modified
klash-summit-scatter
