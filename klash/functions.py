import logging
import shlex
import subprocess
import sys

log = logging.getLogger(__name__)


def exec_checked(cmd: str):
    log.info(f"executing: '{cmd}'")
    result = subprocess.run(shlex.split(cmd), capture_output=False)
    if result.returncode:
        sys.exit(f"Return code: {result.returncode}")


def exec_checked_stdout(cmd: str) -> str:
    log.info(f"executing: '{cmd}'")
    result = subprocess.run(shlex.split(cmd), capture_output=True)
    if result.returncode:
        sys.exit(f"Return code: {result.returncode}")

    return result.stdout.decode('utf-8')


def init_logging():
    logging.basicConfig(level=logging.DEBUG)
