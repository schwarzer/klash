import logging
import os
from pathlib import Path

import klash.constants as c
from klash.functions import exec_checked, init_logging

log = logging.getLogger(__name__)
cwd = Path.cwd()


def main():
    init_logging()

    os.chdir(c.support_path)
    exec_checked(f"pology/bin/posummit scripts/messages.summit {c.lang_code} scatter")
    exec_checked(f"pology/bin/posummit scripts/docmessages.summit {c.lang_code} scatter")

    os.chdir(cwd)
    exec_checked("svn ci -m 'summit scatter'")
