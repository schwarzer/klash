import os

lang_code = 'de'
lang_code_long = 'de_DE.UTF-8'

kde_repo = 'svn+ssh://svn@svn.kde.org/home/kde'
stable_branch = os.path.join('branches', 'stable')
trunk_branch = 'trunk'
stable_l10n_branches = ['l10n-kde4', 'l10n-kf5']
trunk_l10n_branches = ['l10n-support', 'l10n-kf5', 'l10n-kf6']
sub_folders = ['scripts', 'templates', lang_code]

stable_paths = [os.path.join(stable_branch, x) for x in stable_l10n_branches]
trunk_paths = [os.path.join(trunk_branch, x) for x in trunk_l10n_branches]

stable_sub_folders = [os.path.join(stable_branch, x, y) for x in stable_l10n_branches for y in sub_folders]
trunk_sub_folders = [os.path.join(trunk_branch, x, y) for x in trunk_l10n_branches for y in sub_folders]

support_path = os.path.join(trunk_branch, 'l10n-support')
pology_path = os.path.join(support_path, 'pology')

summit_template_path = os.path.join(support_path, 'templates', 'summit')
summit_lang_path = os.path.join(support_path, lang_code, 'summit')
