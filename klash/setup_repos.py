import logging
from pathlib import Path

import klash.constants as c
from klash.functions import exec_checked, init_logging

log = logging.getLogger(__name__)
cwd = Path.cwd()


def main():
    init_logging()

    exec_checked(f"svn co --depth=empty {c.kde_repo} {cwd}")
    exec_checked(f"svn up --depth=empty branches {c.stable_branch} {' '.join(c.stable_paths)}")
    exec_checked(f"svn up --depth=empty {c.trunk_branch} {' '.join(c.trunk_paths)}")
    exec_checked(f"svn up {' '.join(c.stable_sub_folders)}")
    exec_checked(f"svn up {' '.join(c.trunk_sub_folders)}")
    exec_checked(f"git clone https://invent.kde.org/sdk/pology.git {c.pology_path}")
