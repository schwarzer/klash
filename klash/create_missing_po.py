import logging
import re
from pathlib import Path

import klash.constants as c
from klash.functions import exec_checked, init_logging

log = logging.getLogger(__name__)
cwd = Path.cwd()


def main():
    init_logging()

    has_new_file = False

    pot_files = Path(c.summit_template_path).rglob('*.pot')
    po_files = []  # for lokalize call
    for pot_file in pot_files:
        pot_file_str = str(pot_file)
        log.info(pot_file.name)
        # only replacing 'templates' with the lang code seems fragile, so use more context
        po_file_str = pot_file_str.replace('l10n-support/templates/summit',
                                           f"l10n-support/{c.lang_code}/summit")
        po_file_str = re.sub('pot$', 'po', po_file_str)

        po_file = Path(po_file_str)
        if not po_file.is_file():
            log.info(f"+++ Creating: {po_file.name}")
            po_file.parent.mkdir(parents=True, exist_ok=True)
            exec_checked(f"msginit --locale={c.lang_code_long} --no-wrap --no-translator --input={pot_file} "
                         f"--output-file={po_file}")
            exec_checked(f"svn add --parents {po_file}")
            po_files.append(po_file)
            has_new_file = True

    if has_new_file:
        print(f"about to exec: lokalize {' '.join([str(p) for p in po_files])}")
        exec_checked(f"lokalize {' '.join([str(p) for p in po_files])}")
        exec_checked("svn ci -m'add missing PO files'")
