import logging
import os
import shlex
import subprocess

import klash.constants as c
from klash.functions import exec_checked, exec_checked_stdout, init_logging

log = logging.getLogger(__name__)


def po_file_is_finished(file: str) -> bool:
    file_stats = subprocess.run(shlex.split(f"msgfmt --statistics --output=/dev/null {file}"), capture_output=True)
    output_str = ''
    if file_stats.stderr is not None:
        output_str = file_stats.stderr.decode('utf-8')  # msgfmt outputs on STDERR for some reason
    return 'fuzzy' not in output_str and 'untranslated' not in output_str


def main():
    init_logging()

    os.chdir(c.summit_lang_path)

    changed_files_output = exec_checked_stdout('svn diff -rPREV:HEAD --summarize').splitlines(False)

    has_file_to_edit = False
    changed_files = []

    for line_str in changed_files_output:
        file_str = line_str.split(' ')[-1]
        # These are just personal preference. Need to put them in a config file at some point.
        if file_str.startswith('messages') \
                and 'websites' not in file_str \
                and 'documentation' not in file_str \
                and 'digikam' not in file_str \
                and 'rkward' not in file_str \
                and 'skrooge' not in file_str \
                and 'kmymoney' not in file_str \
                and 'krita' not in file_str \
                and 'kstars' not in file_str \
                and 'heaptrack' not in file_str \
                and 'kdev' not in file_str \
                and 'calligra' not in file_str \
                and 'kdenlive' not in file_str \
                and not po_file_is_finished(file_str):
            has_file_to_edit = True
            changed_files.append(file_str)

    if has_file_to_edit:
        print(f"about to exec: lokalize {' '.join([str(p) for p in changed_files])}")
        exec_checked(f"lokalize {' '.join([str(p) for p in changed_files])}")
        exec_checked("svn ci -m 'translation update'")
