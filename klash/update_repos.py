import logging

import klash.constants as c
from klash.functions import exec_checked, init_logging

log = logging.getLogger(__name__)


def main():
    init_logging()

    exec_checked(f"svn up {' '.join(c.stable_sub_folders)}")
    exec_checked(f"svn up {' '.join(c.trunk_sub_folders)}")
    exec_checked(f"git -C {c.pology_path} pull")
