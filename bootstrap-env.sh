#!/usr/bin/env bash

set -ue

if ! [ -d "klash" ]; then
    echo "FATAL: this must be executed in the root of the klash project"
    exit 1
fi

VENV=.venv_klash

rm -rf $VENV
python3 -m venv $VENV
$VENV/bin/pip install --upgrade pip
$VENV/bin/pip install --no-deps --editable .

echo "******************************************************************"
echo "activate the development venv by running"
echo "    source $VENV/bin/activate"
echo "******************************************************************"

