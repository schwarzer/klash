from setuptools import setup, find_packages

setup(
    name="Klash",
    version='0.3.0',
    description='Manage KDE summit workflow',
    url='https://invent.kde.org/schwarzer/klash',
    packages=find_packages(),
    entry_points={'console_scripts': ['klash-setup-repos = klash.setup_repos:main',
                                      'klash-update-repos = klash.update_repos:main',
                                      'klash-summit-merge = klash.summit_merge:main',
                                      'klash-create-missing-po = klash.create_missing_po:main',
                                      'klash-summit-scatter = klash.summit_scatter:main',
                                      'klash-edit-modified = klash.edit_modified:main']
                  }
)
