#!/bin/bash

set -u
set -e

export L10NLANG="de"

./trunk/l10n-support/pology/bin/posieve check-tp-kde trunk/l10n-support/$L10NLANG

