#!/usr/bin/env bash

set -e
set -u

rm -rf branches
rm -rf trunk
rm -rf .svn

