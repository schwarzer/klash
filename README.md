# What is Klash
This is a small collection of scripts for managing the KDE summit workflow. It started as some Bash scripts, made a detour to Perl6 land and is now built in beginner-grade Python. There are a lot of things that can be solved better but it worked so I never found it in me to improve things.

# The Name
Klash stands for **K**DE **L**10N **S**ummit **H**elper with and extra **A** purely for the sound of it.

# Configure
You can configure what language and branches are used in `constants.py`. The message folders used for summit merge and scatter can be configured in `summit_merge.py` and `summit_scatter.py`. Be advised to keep those two in sync.

# Prerequisites
- setup KDE SVN account with SSH key
- install `ssh-askpass` so you do not have to enter your password for every step
- run `bootstap-env.sh` and activate venv as stated by the bootstrap script
- run `klash-setup-repos`
- install Lokalize and setup your summit project

# On a regular basis
- run `run-full-update.sh`

## This executes the following steps

### klash-update-repos
Updates the repos that have been initialised with `klash-setup-repos` before.

### klash-summit-merge
Runs the summit merge process, gathering translation messages from the configured branches into the summit branch.
You can change in `summit_merge.py` what branches are handled. These should be identical to the ones in `summit_scatter.py`.

### create-missing-po
This checks if there are POT files without corresponding PO files and if so, creates those PO files and opens them in Lokalize. This script waits for Lokalize to be closed before continuing.

### edit-modified
This determines which files have been modified since the last `svn up`, checks those files for untranslated and fuzzy strings and opens them in Lokalize. This might open lots of files. The script waits for Lokalize to be closed before continuing.
There are quite some folders excluded from this to avoid opening even more files. See `edit_modified.py`.

### klash-summit-scatter
This runs the summit scatter process, distributing translation messages from the summit branch into the configures branches.
You can change in `summit_scatter.py` what branches are handled. These should be identical to the ones in `summit_merge.py`.

